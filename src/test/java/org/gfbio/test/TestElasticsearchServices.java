package org.gfbio.test;

import static org.junit.Assert.assertEquals;
import java.util.ArrayList;
import java.util.List;
import org.gfbio.main.AppProperties;
import org.gfbio.model.json.IndexDocument;
import org.gfbio.service.ElasticsearchService;
import org.gfbio.utils.ChangeStatus;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import co.elastic.clients.elasticsearch._types.Result;
import co.elastic.clients.elasticsearch.core.BulkResponse;
import co.elastic.clients.elasticsearch.core.DeleteResponse;
import co.elastic.clients.elasticsearch.core.IndexResponse;
import co.elastic.clients.elasticsearch.core.UpdateResponse;

/**
 * Tests need a running Elasticsearch instance (preferably NOT a production environment)
 * 
 * Add @Ignore before build if you want to skip tests
 *
 */
public class TestElasticsearchServices {

  private static ElasticsearchService esService;

  private static final String indexName = "test_lookup_index";

  @BeforeClass
  public static void init() {

    AppProperties appProperties = AppProperties.getInstance();
    appProperties.initProperties("/home/lefko/work/lookup-index/lookup_index_generator.properties");

    esService = new ElasticsearchService();

  }

  @Test
  public void testCreateDocument() {

    List<String> synonyms = new ArrayList<String>();

    IndexDocument testDocument = new IndexDocument("Test Document", "TSTEST",
        "http://terminologies.gfbio.org/test/TestDoc_0001", "Just for testing purposes", null, null,
        null, synonyms, null);

    IndexResponse response =
        esService.createDocument(testDocument, indexName, testDocument.getId());

    assertEquals(1, response.version());
    assertEquals(Result.Created, response.result());

    // delete afterwards
    DeleteResponse resp = esService.deleteDocument(indexName, testDocument.getId());
    assertEquals(Result.Deleted, resp.result());
  }

  @Test
  public void testUpdateDocument() {

    List<String> synonyms = new ArrayList<String>();

    IndexDocument testDocument = new IndexDocument("Test Document 2", "TSTEST",
        "http://terminologies.gfbio.org/test/TestDoc_0002", "Just for testing purposes", null, null,
        null, synonyms, null);

    IndexResponse response =
        esService.createDocument(testDocument, indexName, testDocument.getId());

    // assertEquals(1, response.version());
    assertEquals(Result.Created, response.result());

    synonyms = new ArrayList<String>();
    synonyms.add("check");
    synonyms.add("evaluation");

    IndexDocument testUpdatedDocument = new IndexDocument("Test Document 2", "TSTEST",
        "http://terminologies.gfbio.org/test/TestDoc_0002",
        "Just for testing purposes - this one was updated", null, null, null, synonyms, null);
    testUpdatedDocument.setAuthor("Anon Nymus");

    UpdateResponse uRes =
        esService.updateDocument(testUpdatedDocument, indexName, testUpdatedDocument.getId());

    // assertEquals(2, uRes.version());
    assertEquals(Result.Updated, uRes.result());

    IndexDocument updatedDocument =
        esService.getDocument(indexName, testUpdatedDocument.getId()).source();

    assertEquals("Test Document 2", updatedDocument.getLabel());
    assertEquals("Anon Nymus", updatedDocument.getAuthor());

    // delete afterwards
    DeleteResponse resp = esService.deleteDocument(indexName, testUpdatedDocument.getId());
    assertEquals(Result.Deleted, resp.result());
  }

  @Test
  public void testBulkOperation() {

    BulkResponse response = null;

    List<String> synonyms = new ArrayList<String>();
    synonyms.add("synonym1");
    synonyms.add("synonym2");

    List<String> broaders = new ArrayList<String>();
    broaders.add("broader_term1");
    broaders.add("broader_term2");


    IndexDocument testDocument1 = new IndexDocument("Test Document 3", "TSTEST",
        "http://terminologies.gfbio.org/test/TestDoc_0003",
        "This is a test description for Test Document 3", null, "Alice", null, synonyms, null);

    IndexDocument testDocument2 = new IndexDocument("Test Document 4", "TSTEST",
        "http://terminologies.gfbio.org/test/TestDoc_0004",
        "This is a test description for Test Document 4", null, "Bob", null, synonyms, null);

    IndexDocument testDocument3 = new IndexDocument("Test Document 5", "TSTEST",
        "http://terminologies.gfbio.org/test/TestDoc_0005",
        "This is a test description for Test Document 5", null, null, null, synonyms, null);

    List<IndexDocument> documents = new ArrayList<IndexDocument>();
    documents.add(testDocument1);
    documents.add(testDocument2);
    documents.add(testDocument3);

    // (1) test to add new documents
    response = esService.bulkOperation(documents, indexName, ChangeStatus.ADDED);

    assertEquals(3, response.items().size());
    // assertEquals(1, response.items().get(0).version().intValue());
    assertEquals("created", response.items().get(0).result());
    // assertEquals(1, response.items().get(1).version().intValue());
    assertEquals("created", response.items().get(1).result());
    // assertEquals(1, response.items().get(2).version().intValue());
    assertEquals("created", response.items().get(2).result());

    documents.clear();

    //
    // (2) update existing documents
    //

    testDocument3.setAuthor("Anew Author");
    testDocument3.setLabel("Test Document 5.1");
    testDocument2.setBroaders(broaders);

    documents.add(testDocument3);
    documents.add(testDocument2);

    response = esService.bulkOperation(documents, indexName, ChangeStatus.MODIFIED);

    // assertEquals(2, response.items().size());
    // assertEquals(2, response.items().get(0).version().intValue());
    // assertEquals("updated", response.items().get(0).result());
    // assertEquals(2, response.items().get(1).version().intValue());
    // assertEquals("updated", response.items().get(1).result());

    IndexDocument updatedDocument =
        esService.getDocument(indexName, testDocument3.getId()).source();

    assertEquals("Anew Author", updatedDocument.getAuthor());
    assertEquals("Test Document 5.1", updatedDocument.getLabel());

    // (3) test bulk delete
    documents.add(testDocument1);
    response = esService.bulkOperation(documents, indexName, ChangeStatus.REMOVED);
    assertEquals(3, response.items().size());
    assertEquals("deleted", response.items().get(0).result());
    assertEquals("deleted", response.items().get(1).result());
    assertEquals("deleted", response.items().get(2).result());
  }

  @AfterClass
  public static void testDeleteDocument() {

    List<String> documentsToDelete = new ArrayList<String>();
    documentsToDelete.add("http://terminologies.gfbio.org/test/TestDoc_0003");
    documentsToDelete.add("http://terminologies.gfbio.org/test/TestDoc_0004");
    documentsToDelete.add("http://terminologies.gfbio.org/test/TestDoc_0005");
    documentsToDelete.add("http://terminologies.gfbio.org/test/TestDoc_0002");
    documentsToDelete.add("http://terminologies.gfbio.org/test/TestDoc_0001");

    for (String uri : documentsToDelete)
      esService.deleteDocument(indexName, String.valueOf(uri.hashCode()));
  }
}
