package org.gfbio.test;

import static org.junit.Assert.assertEquals;
import java.util.List;
import org.gfbio.model.term.Term;
import org.gfbio.service.VirtuosoService;
import org.junit.Ignore;
import org.junit.Test;


public class TestVirtuosoServices {

  private VirtuosoService virtService;

  private static final String TEST_URI = "http://purl.obolibrary.org/obo/ENVO_06105012";

  private static final String TEST_ACRONYM = "ENVO";

  @Test
  public void testSynonymExtraction() {
    System.out.println("testSynonymExtraction");

    virtService = new VirtuosoService();
    List<Term> results = virtService.readSynonyms(TEST_ACRONYM, TEST_URI);

    System.out.println(results.get(0));

    assertEquals(3, results.size());

    System.out.println("\n");
  }

  @Test
  public void testCountAllTerms() {

    virtService = new VirtuosoService();
    int count = virtService.countAllTerms("ENVO");

    assertEquals(6566, count);

    assertEquals(1042646, virtService.countAllTerms("ITIS"));
  }

  @Test
  @Ignore
  public void testBroaderTermsExtraction() {
    System.out.println("testBroaderTermsExtraction");

    virtService = new VirtuosoService();
    List<Term> results = virtService.readBroader(TEST_ACRONYM, TEST_URI);

    System.out.println(results.get(0));

    assertEquals(2, results.size());

    System.out.println("\n");
  }

  @Test
  public void testTermsExtraction() {
    System.out.println("testTermsExtraction");

    virtService = new VirtuosoService();
    List<Term> results = virtService.getAllTerms(TEST_ACRONYM);

    assertEquals(6748, results.size());

    String limit = "100", offset = "100";

    results = virtService.getAllTerms(TEST_ACRONYM, limit, offset);

    assertEquals(100, results.size());

    System.out.println("\n");
  }

  @Test
  public void testITISProcessing() {
    System.out.println("testITISProcessing");
    // http://terminologies.gfbio.org/ITIS/Taxa_1113442
    // http://terminologies.gfbio.org/ITIS/Taxa_174247
    // https://www.itis.gov/ITISWebService/services/ITISService/getCommonNamesFromTSN?tsn=183833

    virtService = new VirtuosoService();

    List<Term> results =
        virtService.readBroader("ITIS", "http://terminologies.gfbio.org/ITIS/Taxa_572993");
    assertEquals(18, results.size());
    System.out.println(results.size());

    results = virtService.readSynonyms("ITIS", "http://terminologies.gfbio.org/ITIS/Taxa_572993");
    assertEquals(3, results.size());
    System.out.println(results.size());
  }
}
