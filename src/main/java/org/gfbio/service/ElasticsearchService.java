package org.gfbio.service;

import java.util.List;
import org.gfbio.dao.ElasticsearchAccessDao;
import org.gfbio.model.json.IndexDocument;
import org.gfbio.utils.ChangeStatus;
import co.elastic.clients.elasticsearch.core.BulkResponse;
import co.elastic.clients.elasticsearch.core.DeleteResponse;
import co.elastic.clients.elasticsearch.core.GetResponse;
import co.elastic.clients.elasticsearch.core.IndexResponse;
import co.elastic.clients.elasticsearch.core.UpdateResponse;

public class ElasticsearchService {

  private ElasticsearchAccessDao esDao = new ElasticsearchAccessDao();

  /**
   * 
   * @param document
   * @param index
   * @param id
   * @return
   */
  public IndexResponse createDocument(IndexDocument document, String index, String id) {
    return esDao.createDocument(document, index, id);
  }

  /**
   * 
   * @param index
   * @param id
   * @return
   */
  public GetResponse<IndexDocument> getDocument(String index, String id) {
    return esDao.getDocument(index, id);
  }

  /**
   * 
   * @param document
   * @param index
   * @param id
   * @return
   */
  public UpdateResponse updateDocument(IndexDocument document, String index, String id) {
    return esDao.updateDocument(document, index, id);
  }

  /**
   * 
   * @param index
   * @param id
   * @return
   */
  public DeleteResponse deleteDocument(String index, String id) {
    return esDao.deleteDocument(index, id);
  }

  /**
   * 
   * @param documents
   * @param index
   * @param mode
   * @return
   */
  public BulkResponse bulkOperation(List<IndexDocument> documents, String index,
      ChangeStatus mode) {
    return esDao.bulkOperation(documents, index, mode);
  }

}
